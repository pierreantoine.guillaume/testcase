<?php


namespace pag;


use Exception;

class TestCase extends \PHPUnit\Framework\TestCase
{
    public function catchException(callable $lambda) : ?Exception
    {
        try {
            $lambda();
            return null;
        } catch (Exception $e) {
            return $e;
        }
    }
}
