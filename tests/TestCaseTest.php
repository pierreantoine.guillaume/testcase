<?php

namespace tests\pag;

use pag\TestCase;
use RuntimeException;

class TestCaseTest extends TestCase
{
    /**
     * @covers \pag\TestCase::catchException
     */
    public function testThrow(): void
    {
        # Given
        // $this is the tested object
        # When
        $exception = $this->catchException(fn() => $this->wrapThrow());

        # Then
        $this->assertEquals(new RuntimeException('Message', 13), $exception);
    }

    protected function wrapThrow(): void
    {
        throw new RuntimeException('Message', 13);
    }


    /**
     * @covers \pag\TestCase::catchException
     */
    public function testNoThrow(): void
    {
        # Given
        // $this is the tested object
        # When
        $exception = $this->catchException(
            function () {
            }
        );

        # Then
        $this->assertEquals(null, $exception);
    }
}
